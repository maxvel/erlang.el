(defun to-wide-text (text)
  (interactive "s")
  (apply #'string
   (cl-loop for c in (append text nil)
      collect (cond
   ((and (>= c ?a) (<= c ?z))
    (make-char 'unicode 0 255
   (+ 33 (- c 97))))
   ((= c ? ) (make-char 'unicode 0 48 0))
   (t c)))))

(defun indent-newline-indent ()
  (interactive)
  (indent-for-tab-command)
  (newline-and-indent))

(defun wsclean-file ()
  (interactive)
  (let ((filename (buffer-file-name))
  (exts-to-clean '("erl" "py" "config" "el" "lisp"
       "org" "js" "jsx" "html" "dtl" "pl" "pm")))
    (when (and (stringp filename)
   (member (file-name-extension filename)
     exts-to-clean))
      (whitespace-cleanup))))

(add-hook 'before-save-hook 'wsclean-file)

(defconst *units* '(elpa auto-install web ivy erlang pretty-symbols scratch2 asdf scratch2))
(defmacro defunit (name &rest body)
  `(when (member ',name *units*) ,@body))


(load "server")
(when (and (server-running-p) (daemonp))
  (message "Another instance found, exiting...")
  (kill-emacs))

(require 'cl)

(message (if (daemonp)
       (format "starting emacs-daemon for user %s" (getenv "USER"))
     (format "starting normal emacs")))

(defunit gentoo
  (require 'site-gentoo))

;;; ELPA

(defunit elpa
  (require 'package)
  ;; (if (fboundp 'gnutls-available-p)
  ;;     (fmakunbound 'gnutls-available-p)
  ;;   (setq tls-program '("gnutls-cli --tofu -p %p %h")
  ;;         imap-ssl-program '("gnutls-cli --tofu -p %p %s")
  ;;         smtpmail-stream-type 'starttls
  ;;         starttls-extra-arguments '("--tofu")))

    ;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
  ;; (add-to-list 'package-archives '("marmalade" . "http://marmalade.ferrier.me.uk/"))
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
  (package-initialize)

  ;; (require 'dired+)
  ;; (require 'powerline)
    ;(require 'moe-theme)

  (add-hook 'haskell-mode-hook 'intero-mode)

  ;; (org-babel-do-load-languages 'org-babel-load-languages
  ;;                              '((python . t) (sqlite . t) (sh . t)))
  )



(defunit auto-install
;;; Auto installation
  (let ((my-packages '(
           ace-jump-mode
           better-defaults
           company
           desktop
           flycheck
           helm
           helm-projectile
           magit
           paredit
           projectile
           rainbow-delimiters
           rainbow-identifiers
           smartparens
           web-mode
           which-key
           ivy
           ivy-rich
           amx
           request
           counsel
           yasnippet)))
    (dolist (p my-packages)
      (when (not (package-installed-p p))
    (package-install p))))

;;; Requires

  ;; (require 'git-gutter-fringe)
  ;; (global-git-gutter-mode 1)

  ;; (let ((my-reqs '(better-defaults
  ;;                  hl-line
  ;;                  ;; ido-ubiquitous
  ;;                  ;; smex
  ;;                  )))
  ;;   (dolist (r my-reqs)
  ;;     (require r)))

  ;; (ido-mode nil)
  (global-hl-line-mode t)
  ;;(desktop-save-mode 1)
  (smartparens-global-mode 1)
  (global-prettify-symbols-mode 1)
  (yas-global-mode 1))

(defunit rust

  (add-hook 'rust-mode-hook 'cargo-minor-mode)
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)

  (setq racer-cmd "~/.cargo/bin/racer") ;; Rustup binaries PATH
  (setq racer-rust-src-path "~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src") ;; Rust source code PATH

  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)

  (setq rust-rustfmt-bin "~/.cargo/bin/rustfmt1") ;; Rustup binaries PATH

  (add-hook 'rust-mode-hook
      (lambda ()
  (local-set-key (kbd "C-c <tab>") #'rust-format-buffer)))
  )

(defunit web
  (require 'web-mode)
  (add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.dtl\\'" . web-mode))
  (add-to-list 'web-mode-content-types '("jsx" . "\\.js\\'"))
  (add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.inc\\'" . web-mode))

  (setq web-mode-engines-alist
  '(("mason"    . "\\.html\\'")
    ("mason"  . "\\.js\\.")
    ("mason"  . "\\.comp\\.")))

  (add-hook 'web-mode-hook 'smartparens-mode))

(global-set-key (kbd "C-x g") 'magit-status)

(defunit helm
  (require 'helm)
  (require 'helm-config)

  (global-set-key (kbd "C-x C-r") 'helm-recentf)
  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-x b") 'helm-mini)
  (setq helm-buffers-fuzzy-matching t
  helm-recentf-fuzzy-match    t
  helm-M-x-fuzzy-match t)

  (global-set-key (kbd "M-y") 'helm-show-kill-ring)
  (helm-mode 1))

(defunit ivy
  (require 'ivy)
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (global-set-key (kbd "C-s") 'swiper)
  (global-set-key (kbd "C-s") 'swiper)
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
  (global-set-key (kbd "C-x d") 'counsel-find-file)
  (global-set-key (kbd "C-x C-d") 'counsel-find-file)
  (global-set-key (kbd "C-x C-r") 'counsel-recentf)
  (global-set-key (kbd "<f1> f") 'counsel-describe-function)
  (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
  (global-set-key (kbd "<f1> l") 'counsel-find-library)
  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
  (require 'ivy-rich)
  ;; (ivy-set-display-transformer 'ivy-switch-buffer 'ivy-rich-switch-buffer-transformer)
)

;;; Projectile
(defunit projectile
  (projectile-global-mode)
  (defunit helm
    (setq projectile-completion-system 'helm)
    (helm-projectile-on)))

;; (global-set-key [(meta x)] (lambda ()
;;                              (interactive)
;;                              (or (boundp 'smex-cache)
;;                                  (smex-initialize))
;;                              (global-set-key [(meta x)] 'smex)
;;                              (smex)))

;; (define-key global-map (kbd "C-x SPC") 'ace-jump-mode)

;;; Remove linum from chats

(mapc (lambda (mode)
  (add-hook mode (lambda ()
       (linum-mode 0))))
      '(erc-mode-hook
  jabber-chat-mode-hook
  eshell-mode-hook
  speedbar-mode-hook))


;;; X Clipboard

;; (setq x-select-enable-primary t
;;       x-select-enable-clipboard t)

;; OCaml
(defunit ocaml
  ;; Setup environment variables using opam
  ;; (dolist (var (car (read-from-string (shell-command-to-string "opam config env --sexp"))))
  ;;   (setenv (car var) (cadr var)))

  ;; Update the emacs path
  (setq exec-path (append (parse-colon-path (getenv "PATH"))
  (list exec-directory)))

  ;; Update the emacs load path
  ;; (add-to-list 'load-path (expand-file-name "../../share/emacs/site-lisp"
  ;;                                           (getenv "OCAML_TOPLEVEL_PATH")))

  ;; Automatically load utop.el
  ;; (autoload 'utop "utop" "Toplevel for OCaml" t)
  ;; (autoload 'utop-setup-ocaml-buffer "utop" "Toplevel for OCaml" t)
  ;; (add-hook 'tuareg-mode-hook 'utop-setup-ocaml-buffer)
  )

(defun shell-command+ (cmd &optional error)
  (with-temp-buffer
    (let ((code (shell-command cmd (current-buffer)))
      (result (string-trim-right (buffer-string))))
      (when (and error (not (zerop code)))
    (error result))
      (list code result))))


(defunit asdf
  (require 'cl)
  (require 'dash)
  (require 's)

  (cl-defun asdf--shell-command (cmd &optional (signal-error t))
    (with-temp-buffer
      (let ((code (shell-command cmd (current-buffer)))
        (result (string-trim (buffer-string))))
    (cond
     ((and signal-error (not (zerop code))) (error result))
     (signal-error result)
     (t (list code result))))))

  (defmacro def-asdf-command (asdf-cmd args docstring)
    (let* ((full-command (s-concat "asdf " asdf-cmd))
       (fun-name (intern (concat "asdf-"
                     (s-replace-all '((" " . "")) asdf-cmd))))
       (is-optional-arg? (lambda (s) (s-ends-with? "?" (symbol-name s))))
       (opt-args (mapcar (lambda (x) (list x (funcall is-optional-arg? x))) args))
       (interactive-string (s-join "\n" (mapcar (lambda (x) (concat "s" (s-capitalize (symbol-name x)) ": ")) args))))
      ;; (message (format "opts-args: %s" opt-args))
      `(defun ,fun-name ,args
     ,docstring
     (interactive ,interactive-string)
     ;; check if we have all non-optional arguments
     (let ((args
        (cl-loop
         for arg in (quote ,args)
         when (let ((is-optional (second (assoc arg (quote ,opt-args))))
                (is-empty (seq-empty-p (symbol-value arg))))
            (when (and (not is-optional) is-empty)
              (user-error (format "ASDF error: argument '%s' to %s must not be empty!" arg (quote ,fun-name))))
            (not is-empty))
         collect (symbol-value arg))))
       ;; (message (format "args: %s" args))
       (asdf--shell-command (s-join " " (cons ,full-command args)))))))


  (def-asdf-command "plugin-add" (name git-url?) "Add a plugin from the plugin repo OR, add a Git repo as a plugin by specifying the name and repo url")
  (def-asdf-command "plugin-list" () "List installed plugins")
  (def-asdf-command "plugin-list --urls" () "List installed plugins with repository URLs")
  (def-asdf-command "plugin-list-all" () "List plugins registered on asdf-plugins repository with URLs")
  (def-asdf-command "plugin-remove" (name) "Remove plugin and package versions")
  (def-asdf-command "plugin-update" (name) "Update plugin")
  (def-asdf-command "plugin-update --all" () "Update all plugins")

  (def-asdf-command "install" (name version) "Install a specific version of a package")
  (def-asdf-command "uninstall" (name version) "Remove a specific version of a package")
  (def-asdf-command "current" (name?) "Display current version set or being used for all packages")
  (def-asdf-command "where" (name version?) "Display install path for an installed or current version")
  (def-asdf-command "which" (name) "Display install path for current version")
  (def-asdf-command "local" (name version) "Set the package local version")
  (def-asdf-command "global" (name version) "Set the package global version")
  (def-asdf-command "list" (name) "List installed versions of a package")
  (def-asdf-command "list-all" (name) "List all versions of a package")

  (def-asdf-command "reshim" (name version) "Recreate shims for version of a package")
  (def-asdf-command "update" () "Update asdf to the latest stable release")
  (def-asdf-command "update --head" () "Update asdf to the latest on the master branch")
 )

;;; Erlang
(defunit erlang

  (defun asdf-current (name)
    (let* ((cmd (concat "asdf current " name))
       (out (shell-command+ cmd t)))
      (first (split-string (second out) " "))))

  (defun asdf-where (name)
    (let* ((cmd (concat "asdf where " name))
       (out (shell-command+ cmd t)))
      (second out)))

  (setq erlang-root-dir (asdf-where "erlang"))
  (setq elixir-root-dir (asdf-where "elixir"))

  (setq erlang-man-root-dir (concat erlang-root-dir "/man"))

  (let ((tools-dir (shell-command-to-string (concat "ls -d" erlang-root-dir "/lib/tools-*"))))
    (add-to-list 'load-path (string-trim-right tools-dir)))

  (add-to-list 'auto-mode-alist '("\\.erl\\'" . erlang-mode))

  ;;; Indents

  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)
  (setq erlang-indent-level 2)

  ;; (let ((home-bin (concat (expand-file-name "~") "/bin"))
  ;;       (erlang-bin (concat erlang-root-dir "/bin"))
  ;;       (elixir-bin (concat elixir-root-dir "/bin")))
  ;;   (add-to-list 'exec-path home-bin)
  ;;   (add-to-list 'exec-path erlang-bin)
  ;;   (add-to-list 'exec-path elixir-bin)
  ;;   (setenv "PATH" (concat (getenv "PATH") ":" home-bin ":" erlang-bin ":" elixir-bin)))

  (require 'edts-start)

  ;; (defun my-after-init-hook ()
  ;;   (message "flag3")
  ;;   (message "%s" exec-path)
  ;;   (require 'edts-start))

  ;; (add-hook 'after-init-hook 'my-after-init-hook)

  (require 'flycheck)

  ;; (add-hook 'erlang-mode-hook 'flycheck-mode)
  (add-hook 'erlang-mode-hook 'auto-highlight-symbol-mode)
  ;; (add-hook 'erlang-mode-hook 'company-mode)

  ;; (flycheck-define-checker erlang-otp
  ;;                        "An Erlang syntax checker using the Erlang interpreter."
  ;;                        :command ("erlc" "-o" temporary-directory "-Wall"
  ;;                                  "-I" "../include" "-I" "../../include"
  ;;                                  "-I" "../../../include" source)
  ;;                        :error-patterns
  ;;                        ((warning line-start (file-name) ":" line ": Warning:" (message) line-end)
  ;;                         (error line-start (file-name) ":" line ": " (message) line-end)))

  ;; (add-hook 'erlang-mode-hook
  ;;           (lambda ()
  ;;             (flycheck-select-checker 'erlang-otp)
  ;;             (flycheck-mode)))

  ;; (setq exec-path (append (parse-colon-path (getenv "PATH"))
  ;;                         (list exec-directory)))


  ;; (add-to-list 'load-path "/usr/lib/erlang/lib/tools-2.6.11/emacs")
  ;; (add-to-list 'load-path "/usr/lib/erlang/lib/tools-2.6.11/emacs")

  (require 'erlang-start)
  ;; (setq edts-inhibit-package-check t)
  ;; (add-to-list 'load-path "~/.emacs.d/distel/elisp")
  ;; (require 'distel)
  ;; (distel-setup)

  ;; (require 'company-distel)
  ;; (add-to-list 'company-backends 'company-distel)

  ;; (setq distel-completion-get-doc-from-internet t)
  ;; (setq distel-completion-valid-syntax "a-zA-Z:_-")

  ;; prevent annoying hang-on-compile
  ;; (defvar inferior-erlang-prompt-timeout t)
  ;; ;; default node name to emacs@localhost
  ;; ;; (setq inferior-erlang-machine-options '("-sname" "emacs"))
  ;; ;; tell distel to default to that node
  ;; (setq erl-nodename-cache
  ;;       (make-symbol
  ;;        (concat
  ;;         "emacs@"
  ;;         ;; Mac OS X uses "name.local" instead of "name", this should work
  ;;         ;; pretty much anywhere without having to muck with NetInfo
  ;;         ;; ... but I only tested it on Mac OS X.
  ;;         (car (split-string (shell-command-to-string "hostname"))))))



  ;; (require 'edts-start)

  (add-hook 'erlang-mode-hook 'rainbow-delimiters-mode)
  ;; (add-hook 'erlang-mode-hook 'edts-mode)
  (add-hook 'erlang-mode-hook 'smartparens-mode)

  (add-hook 'erlang-mode-hook
      (lambda () (local-set-key (kbd "<return>")
   #'indent-newline-indent))))

(defunit elixir
  (add-hook 'elixir-mode-hook 'company-mode)
  (add-hook 'elixir-mode-hook 'smartparens-mode)
  (add-hook 'elixir-mode-hook 'rainbow-delimiters-mode)

  (add-hook 'elixir-mode-hook
      (lambda () (local-set-key (kbd "C-c C-c")
           #'alchemist-iex-compile-this-buffer-and-go)))


  ;; projectile projects
  (projectile-register-project-type 'rebar '("rebar.config")
            :compile "rebar3 compile"
            :test "rebar3 ct"
            :test-suffix "_SUITE")


  ;; (setq alchemist-mix-command "/usr/local/bin/mix")
  ;; (setq alchemist-mix-test-task "espec")
  ;; (setq alchemist-mix-test-default-options '()) ;; default
  ;; (setq alchemist-iex-program-name "/usr/local/bin/iex") ;; default: iex
  ;; (setq alchemist-execute-command "/usr/local/bin/elixir") ;; default: elixir
  ;; (setq alchemist-compile-command "/usr/local/bin/elixirc") ;; default: elixirc
  ;; (setq alchemist-hooks-test-on-save t)
  ;; (setq alchemist-hooks-compile-on-save t)
  )

;;; Lisps and Clojure
(defunit lisp
  (load (expand-file-name "~/quicklisp/slime-helper.el"))
  ;; Replace "sbcl" with the path to your implementation
  (setq inferior-lisp-program "sbcl")
  ;; (setq inferior-lisp-program "ecl.sh")

  (eval-after-load "slime"
    '(progn
       (slime-setup '(slime-fancy slime-banner))
       (setq slime-complete-symbol*-fancy t)
       (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol))))

(defunit rebol
  (defun rebol-run-script ()
    (interactive)
    (save-buffer)
    (compile (concat "rebol-view " (buffer-file-name))))
  (defun red-run-script ()
    (interactive)
    (save-buffer)
    (compile (concat "wine " (expand-file-name "~/bin/red.exe") " " (buffer-file-name)) t))
  (load-file "~/.emacs.d/rebol.el")
  (load-file "~/.emacs.d/red.el")
  (add-to-list 'auto-mode-alist '("\\.r\\'" . rebol-mode))
  (add-to-list 'auto-mode-alist '("\\.topaz\\'" . rebol-mode))
  (eval-after-load 'rebol
    '(define-key rebol-mode-map (kbd "C-c C-c") 'rebol-run-script))
  (eval-after-load 'red
    '(define-key red-mode-map (kbd "C-c C-c") 'red-run-script)))




;; GAMBIT
(defunit gambit
  ;; (add-to-list 'load-path "~/bin/gambc-v4_7_0-devel/misc")
  ;; (require 'gambit)
  )

;; LFE
(defunit lfe
  (add-to-list 'load-path (expand-file-name "~/dev/lfe/emacs/"))
  (require 'lfe-start)
  (setq inferior-lfe-program-options
  (list "-pa" (expand-file-name "~/dev/lfe/ebin"))))


(defunit clojure
  (add-hook 'clojure-mode-hook 'esk-pretty-fn)
  (add-hook 'clojurescript-mode-hook 'esk-pretty-fn))

(eval-after-load 'paredit
  ;; need a binding that works in the terminal
  '(progn
     (define-key paredit-mode-map (kbd "M-)") 'paredit-forward-slurp-sexp)
     (define-key paredit-mode-map (kbd "M-(") 'paredit-backward-slurp-sexp)))

;(define-key

(eval-after-load 'flycheck
  '(progn
     (define-key flycheck-mode-map
       (kbd "M-p") 'flycheck-tip-cycle-reverse)
     (define-key flycheck-mode-map
       (kbd "M-n") 'flycheck-tip-cycle)))

;; (dolist (mode '(scheme emacs-lisp lisp clojure clojurescript))
;;   (when (> (display-color-cells) 8)
;;     (font-lock-add-keywords (intern (concat (symbol-name mode) "-mode"))
;;                             '(("(\\|)" . 'esk-paren-face))))
;;   (add-hook (intern (concat (symbol-name mode) "-mode-hook"))
;;             'smartparens-mode))
;;             ;;'paredit-mode))

;;; Customs

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(blink-cursor-mode nil)
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "c59857e3e950131e0c17c65711f1812d20a54b829115b7c522672ae6ba0864cc" "3b903d4b0a528b92e7ec980568a47d47ee0cb3c56acd156340757817e2ddf1e5" "74278d14b7d5cf691c4d846a4bbf6e62d32104986f104c1e61f718f9669ec04b" "6bf237d23440fb0b340f4336695f2a08c6b785aa98288b3313526e76c38bca19" "5e3fc08bcadce4c6785fc49be686a4a82a356db569f55d411258984e952f194a" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "9f443833deb3412a34d2d2c912247349d4bd1b09e0f5eaba11a3ea7872892000" default)))
 '(diredful-mode t)
 '(display-time-24hr-format t)
 '(display-time-mode t)
 '(edts-man-root "~/.emacs.d/edts/doc/21.1")
 '(erlang-electric-newline-inhibit t)
 '(erlang-electric-semicolon-insert-blank-lines 1)
 '(erlang-new-clause-with-arguments t)
 '(fci-rule-color "#eee8d5")
 '(find-name-arg "-iname")
 '(frame-brackground-mode (quote dark))
 '(fringe-mode 6 nil (fringe))
 '(global-linum-mode t)
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#fdf6e3" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#586e75")
 '(highlight-tail-colors
   (quote
    (("#eee8d5" . 0)
     ("#B4C342" . 20)
     ("#69CABF" . 30)
     ("#69B7F0" . 50)
     ("#DEB542" . 60)
     ("#F2804F" . 70)
     ("#F771AC" . 85)
     ("#eee8d5" . 100))))
 '(hl-bg-colors
   (quote
    ("#DEB542" "#F2804F" "#FF6E64" "#F771AC" "#9EA0E5" "#69B7F0" "#69CABF" "#B4C342")))
 '(hl-fg-colors
   (quote
    ("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3")))
 '(ivy-action-wrap t)
 '(ivy-wrap t)
 '(linum-format "%4d ")
 '(magit-diff-arguments
   (quote
    ("--ignore-space-change" "--ignore-all-space" "--no-ext-diff")))
 '(magit-diff-section-arguments (quote ("--no-ext-diff")))
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-babel-exp-code-template
   "#+BEGIN_SRC %lang%switches%flags
=<%name>

%body
#+END_SRC")
 '(package-selected-packages
   (quote
    (erlstack-mode py-autopep8 restclient projectile-speedbar edts alchemist ioccur auto-highlight-symbol flycheck-dialyzer flycheck-rebar3 flycheck-tip ag company-distel yasnippet counsel request amx ivy-rich ivy which-key web-mode smartparens rainbow-identifiers rainbow-delimiters paredit magit helm-projectile helm flycheck company better-defaults ace-jump-mode)))
 '(pos-tip-background-color "#eee8d5")
 '(pos-tip-foreground-color "#586e75")
 '(savehist-mode t)
 '(show-paren-mode t)
 '(slime-autodoc-use-multiline-p t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(solarized-contrast (quote high))
 '(speedbar-show-unknown-files t)
 '(sr-speedbar-max-width 280)
 '(sr-speedbar-width-x 200)
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#fdf6e3" "#eee8d5" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#657b83" "#839496")))
 '(xterm-color-names
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#073642"])
 '(xterm-color-names-bright
   ["#fdf6e3" "#cb4b16" "#93a1a1" "#839496" "#657b83" "#6c71c4" "#586e75" "#002b36"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "simp" :slant normal :weight normal :height 120 :width normal)))))

;;; Theme

;; background #fdf6e3
;; hl-line-background: #eee8d5

;; (set-face-background 'hl-line "#eee8d5")
;; (set-face-background 'default "#fdf6e3")
;; (set-face-background 'cursor "#657b83")


;; (defface esk-paren-face
;;   '((((class color) (background dark))
;;      (:foreground "grey50"))
;;     (((class color) (background light))
;;      (:foreground "grey55")))
;;   "Face for parentheses"
;;   :group 'starter-kit-faces)

(load-theme 'tango-dark)

(set-face-background 'hl-line "#3e4446")
(set-face-foreground 'highlight nil)

;(set-face-foreground 'highlight nil)

(message "start successful")

;; (projectile-register-project-type
;;  'project
;;  '("file1" "file2") "command")

(require 'json)
(require 'request)


(defunit fira
  (defun fira-code-mode--make-alist (list)
    "Generate prettify-symbols alist from LIST."
    (let ((idx -1))
      (mapcar
       (lambda (s)
     (setq idx (1+ idx))
     (let* ((code (+ #Xe100 idx))
    (width (string-width s))
    (prefix ())
    (suffix '(?\s (Br . Br)))
    (n 1))
       (while (< n width)
     (setq prefix (append prefix '(?\s (Br . Bl))))
     (setq n (1+ n)))
       (cons s (append prefix suffix (list (decode-char 'ucs code))))))
       list)))

  (defconst fira-code-mode--ligatures
    '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\"
      "{-" "[]" "::" ":::" ":=" "!!" "!=" "!==" "-}"
      "--" "---" "-->" "->" "->>" "-<" "-<<" "-~"
      "#{" "#[" "##" "###" "####" "#(" "#?" "#_" "#_("
      ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*"
      "/**" "/=" "/==" "/>" "//" "///" "&&" "||" "||="
      "|=" "|>" "^=" "$>" "++" "+++" "+>" "=:=" "=="
      "===" "==>" "=>" "=>>" "<=" "=<<" "=/=" ">-" ">="
      ">=>" ">>" ">>-" ">>=" ">>>" "<*" "<*>" "<|" "<|>"
      "<$" "<$>" "<!--" "<-" "<--" "<->" "<+" "<+>" "<="
      "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<" "<~"
      "<~~" "</" "</>" "~@" "~-" "~=" "~>" "~~" "~~>" "%%"
      "x" ":" "+" "+" "*"))



  (defvar fira-code-mode--old-prettify-alist)

  (defun fira-code-mode--enable ()
    "Enable Fira Code ligatures in current buffer."
    (setq-local fira-code-mode--old-prettify-alist prettify-symbols-alist)
    (setq-local prettify-symbols-alist (append (fira-code-mode--make-alist fira-code-mode--ligatures) fira-code-mode--old-prettify-alist))
    (prettify-symbols-mode t))

  (defun fira-code-mode--disable ()
    "Disable Fira Code ligatures in current buffer."
    (setq-local prettify-symbols-alist fira-code-mode--old-prettify-alist)
    (prettify-symbols-mode -1))

  (define-minor-mode fira-code-mode
    "Fira Code ligatures minor mode"
    :lighter " FiraCode"
    (setq-local prettify-symbols-unprettify-at-point 'right-edge)
    (if fira-code-mode
    (fira-code-mode--enable)
      (fira-code-mode--disable)))

  (defun fira-code-mode--setup ()
    "Setup Fira Code Symbols"
    (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))
  )


(defunit pretty-symbols
  ;; (defun esk-pretty-lambdas ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("(?\\(lambda\\>\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     ,(make-char 'greek-iso8859-7 107))
  ;;                     nil))))))

  ;; (add-hook 'prog-mode-hook 'esk-pretty-lambdas)

  ;; (defun esk-pretty-fun ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("\\(\\<fun\\>\\)("
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     ,(make-char 'greek-iso8859-7 107)
  ;;                                     ;;"\u0192"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-fn ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("(\\(\\<fn\\>\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u0192"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-rarrow ()
  ;;   (font-lock-add-keywords
  ;;    nil `((" \\(->\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     " \u2192"
  ;;                                     'decompose-region)))))))

  ;; (add-hook 'prog-mode-hook 'esk-pretty-rarrow)

  ;; (defun esk-pretty-larrow ()
  ;;   (font-lock-add-keywords
  ;;    nil `((" \\(<-\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     " \u2190"
  ;;                                     ;; " \u2208"
  ;;                                     'decompose-region)))))))

  ;; (add-hook 'prog-mode-hook 'esk-pretty-larrow)


  ;; (defun esk-pretty-ge ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("[^>]\\(>=\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u2265"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-le ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("\\(=<\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u2264"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-vertbar ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("\\(||\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u2758"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-noteq ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("[^=]\\(/=\\)"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u2260"
  ;;                                     'decompose-region)))))))

  ;; (defun esk-pretty-efn ()
  ;;   (font-lock-add-keywords
  ;;    nil `(("\\(fun\\>\\) *[^(]"
  ;;           (0 (progn (compose-region (match-beginning 1)
  ;;                                     (match-end 1)
  ;;                                     "\u0192"
  ;;                                     'decompose-region)))))))


  (defun prettify-symbols-init ()
    (push '("->" . 8594) prettify-symbols-alist)
    (push '("<-" . 8592) prettify-symbols-alist)
    (push '("<=" . 8656) prettify-symbols-alist)
    (push '("=>" . 8658) prettify-symbols-alist)

    ;; (push '("==" . #X003d) prettify-symbols-alist)
    (push '("/=" . #X2260) prettify-symbols-alist)
    ;; (push '("=:=" . #X2261) prettify-symbols-alist)
    ;; (push '("=/=" . #X2262) prettify-symbols-alist)
    (push '("<" . #X003c) prettify-symbols-alist)
    (push '(">" . #X003e) prettify-symbols-alist)
    (push '("=<" . #X2264) prettify-symbols-alist)
    (push '(">=" . #X2265) prettify-symbols-alist)
    ;; (push '("not" . #X00AC) prettify-symbols-alist)
    (push '("||" . #X2551) prettify-symbols-alist)
    (push '("fun" . 402) prettify-symbols-alist)
    ;; (push '(">>" . 187) prettify-symbols-alist)
    ;; (push '("<<" . 171) prettify-symbols-alist)
    prettify-symbols-mode)

  (add-hook 'prog-mode-hook 'prettify-symbols-init)
  ;; (add-hook 'erlang-mode-hook 'prettify-symbols-init)
  )


(defunit scratch2

  (define-minor-mode s2-mode "Scratch2 mode" :lighter " S2" :global nil)

  (defconst s2-timer nil)
  (defconst s2-home (f-join user-emacs-directory "scratch2/"))

  (defun s2-add-mode-line ()
    (save-excursion
      (goto-char 0)
      (when (s-contains? "-*- mode:" (thing-at-point 'line t))
    (kill-line)
    (kill-line))
      (add-file-local-variable-prop-line 'mode (intern (s-chop-suffix "-mode" (symbol-name major-mode))))))

  (defun s2-init ()
    (unless (file-directory-p s2-home)
      (make-directory s2-home t)))

  (defun s2-buffers ()
    (interactive)
    (ivy-read "scratch: " (mapcar #'buffer-name (s2-list-buffers))
          :action '(lambda (buff)
             (switch-to-buffer (get-buffer buff)))))

  (defun s2-list-buffers ()
    (cl-loop for b in (buffer-list)
         when (with-current-buffer b s2-mode)
         collect b))

  (defun s2-timer-callback ()
    (cl-loop for b in (s2-list-buffers)
     do (with-current-buffer b
          (let ((name (concat s2-home (buffer-name))))
        (s2-add-mode-line)
        (message "*s2* saving buffer: (%s)" name)
        (write-region nil nil name)))))

  (setq s2-timer (run-with-idle-timer 60 t #'s2-timer-callback))

  (defun s2-load-all ()
    (interactive)
    (let ((files (directory-files s2-home t "^[^.]")))
      (cl-loop for f in files
           do (let ((buff (find-file-noselect f)))
            (with-current-buffer buff (s2-mode 1))))))

  (setq projectile-speedbar--last-seen-buffer nil)

  (defun projectile-speedbar--after-refresh ()
    (when (and (not (sr-speedbar-window-p))
           (projectile-project-root)
           (not (eq (current-buffer) projectile-speedbar--last-seen-buffer)))
      (setq projectile-speedbar--last-seen-buffer (current-buffer))
      (projectile-speedbar-open-current-buffer-in-tree)
      ))

  (advice-add 'sr-speedbar-refresh :after #'projectile-speedbar--after-refresh)

  (defun projectile-speedbar-switch ()
    (interactive)
    (if (sr-speedbar-window-p)
    ;; (other-window)
    (switch-to-buffer-other-window projectile-speedbar--last-seen-buffer )
      ;; (switch-to-buffer projectile-speedbar--last-seen-buffer)
      (progn
    (setq projectile-speedbar--last-seen-buffer (current-buffer))
    (sr-speedbar-select-window))))

  (global-set-key (kbd "C-<tab>") 'projectile-speedbar-switch)


  (s2-init)
  (s2-load-all)
  )
